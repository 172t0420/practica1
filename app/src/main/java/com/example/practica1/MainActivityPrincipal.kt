package com.example.practica1

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.io.InputStream


class MainActivityPrincipal : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnClickListener{
            if(rb_mas.isChecked == true) {
                val intent: Intent = Intent(this, MainActivity2::class.java)
                var sexo: String = rb_mas.text.toString()
                intent.putExtra("sexo", sexo)
                var nombre: String = et_nombre.text.toString()
                intent.putExtra("nombre", nombre)
                var apellido: String = et_apellidos.text.toString()
                intent.putExtra("apellido", apellido)
                var edad: String = et_edad.text.toString()
                intent.putExtra("edad", edad)
                if (nombre.equals("") || apellido.equals("") || edad.equals("")) {
                    Toast.makeText(this, "Ingrese los datos faltantes ", Toast.LENGTH_SHORT).show()
                } else {
                    startActivity(intent)
                }
            }
            else{
                val intent: Intent = Intent(this, MainActivity2::class.java)
                var sexo: String = rb_fem.text.toString()
                intent.putExtra("sexo", sexo)
                var nombre: String = et_nombre.text.toString()
                intent.putExtra("nombre", nombre)
                var apellido: String = et_apellidos.text.toString()
                intent.putExtra("apellido", apellido)
                var edad: String = et_edad.text.toString()
                intent.putExtra("edad", edad)
                if (nombre.equals("") || apellido.equals("") || edad.equals("")) {
                    Toast.makeText(this, "Ingrese los datos faltantes ", Toast.LENGTH_SHORT).show()
                } else {
                    startActivity(intent)
                }
            }
            }
        }
    }