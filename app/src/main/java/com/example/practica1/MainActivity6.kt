package com.example.practica1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main6.*

class MainActivity6 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main6)

        val objetoIntent: Intent =intent

        var nombre = objetoIntent.getStringExtra("nombre")
        var apellido = objetoIntent.getStringExtra("apellido")
        var edad = objetoIntent.getStringExtra("edad")
        var sexo = objetoIntent.getStringExtra("sexo")

        boton_derecha.setOnClickListener{
            val intent: Intent = Intent(this, MainActivity7::class.java)
            intent.putExtra("sexo", sexo)
            intent.putExtra("nombre", nombre)
            intent.putExtra("apellido", apellido)
            intent.putExtra("edad", edad)
            startActivity(intent)
        }
        boton_izquierda.setOnClickListener{
            val intent: Intent = Intent(this, MainActivity5::class.java)
            intent.putExtra("sexo", sexo)
            intent.putExtra("nombre", nombre)
            intent.putExtra("apellido", apellido)
            intent.putExtra("edad", edad)
            startActivity(intent)
        }
    }
}