package com.example.practica1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main2.*

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        val objetoIntent: Intent =intent

        var nombre = objetoIntent.getStringExtra("nombre")
        txt_nombre.text = "$nombre"
        var apellido = objetoIntent.getStringExtra("apellido")
        txt_apellido.text = "$apellido"
        var edad = objetoIntent.getStringExtra("edad")
        txt_edad.text = "$edad"
        var sexo = objetoIntent.getStringExtra("sexo")
        txt_sexo.text = "$sexo"

        boton_cuento1.setOnClickListener{
            val intent: Intent = Intent(this, MainActivity3::class.java)
            intent.putExtra("sexo", sexo)
            intent.putExtra("nombre", nombre)
            intent.putExtra("apellido", apellido)
            intent.putExtra("edad", edad)
            startActivity(intent)
        }
        boton_cuento2.setOnClickListener{
            val intent: Intent = Intent(this, MainActivity8::class.java)
            intent.putExtra("sexo", sexo)
            intent.putExtra("nombre", nombre)
            intent.putExtra("apellido", apellido)
            intent.putExtra("edad", edad)
            startActivity(intent)
        }
        boton_cuento3.setOnClickListener{
            val intent: Intent = Intent(this, MainActivity14::class.java)
            intent.putExtra("sexo", sexo)
            intent.putExtra("nombre", nombre)
            intent.putExtra("apellido", apellido)
            intent.putExtra("edad", edad)
            startActivity(intent)
        }
        boton_salir.setOnClickListener{
            val intent: Intent = Intent(this, MainActivityPrincipal::class.java)
            intent.putExtra("sexo", sexo)
            intent.putExtra("nombre", nombre)
            intent.putExtra("apellido", apellido)
            intent.putExtra("edad", edad)
            startActivity(intent)
        }
    }
}