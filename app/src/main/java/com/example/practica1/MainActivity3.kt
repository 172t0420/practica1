package com.example.practica1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main2.*
import kotlinx.android.synthetic.main.activity_main3.*

class MainActivity3 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main3)

        val objetoIntent: Intent =intent

        var nombre = objetoIntent.getStringExtra("nombre")
        var apellido = objetoIntent.getStringExtra("apellido")
        var edad = objetoIntent.getStringExtra("edad")
        var sexo = objetoIntent.getStringExtra("sexo")

        boton_derecha.setOnClickListener{
            val intent: Intent = Intent(this, MainActivity4::class.java)
            intent.putExtra("sexo", sexo)
            intent.putExtra("nombre", nombre)
            intent.putExtra("apellido", apellido)
            intent.putExtra("edad", edad)
            startActivity(intent)
        }
        boton_menu.setOnClickListener{
            val intent: Intent = Intent(this, MainActivity2::class.java)
            intent.putExtra("sexo", sexo)
            intent.putExtra("nombre", nombre)
            intent.putExtra("apellido", apellido)
            intent.putExtra("edad", edad)
            startActivity(intent)
        }
    }



}