package com.example.practica1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main7.*

class MainActivity7 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main7)

        val objetoIntent: Intent =intent

        var nombre = objetoIntent.getStringExtra("nombre")
        var apellido = objetoIntent.getStringExtra("apellido")
        var edad = objetoIntent.getStringExtra("edad")
        var sexo = objetoIntent.getStringExtra("sexo")

        button2.setOnClickListener{
            val intent: Intent = Intent(this, MainActivity6::class.java)
            intent.putExtra("sexo", sexo)
            intent.putExtra("nombre", nombre)
            intent.putExtra("apellido", apellido)
            intent.putExtra("edad", edad)
            startActivity(intent)
        }

        boton_ter.setOnClickListener{
            val intent: Intent = Intent(this, MainActivity2::class.java)
            intent.putExtra("sexo", sexo)
            intent.putExtra("nombre", nombre)
            intent.putExtra("apellido", apellido)
            intent.putExtra("edad", edad)
            startActivity(intent)
        }
    }
}